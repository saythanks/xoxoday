<?php

namespace SayThanks\Xoxoday;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayThanks\Xoxoday\Exceptions\InvalidConfigException;

class Xoxoday
{

    public string $host;
    private string $client_id;
    private string $client_secret;

    public function __construct()
    {
        $this->host = config('xoxoday.host');
        $this->client_id = config('xoxoday.auth.client_id');
        $this->client_secret = config('xoxoday.auth.client_secret');

        $this->validateConfig();
    }

    /**
     *  External API Calls
     */
    public function getOrderDetails($orderId, $poNumber = null, bool $sendMailToReciever = false)
    {
        $data = [
            'query' => 'plumProAPI.mutation.getOrderDetails',
            'variables' =>
                [
                    'data' =>
                        [
                            'poNumber' => $poNumber,
                            'orderId' => $orderId,
                            'sendMailToReceiver' => $sendMailToReciever ? 1 : null,
                        ]
                ],
        ];
        return $this->post($data);
    }

    public function getBalance()
    {
        $data = [
            'query' => 'plumProAPI.mutation.getBalance',
        ];
        return $this->post($data);
    }

    public function getVouchers(int $limit = 10, int $page = 0, string $productName = null, string $country = null, string $price = null, string $minPrice = null, string $maxPrice = null, string $currencyCode = null, string $includeProducts = null, string $excludeProducts = null)
    {
        // Stub
        $data = [
            'query' => 'plumProAPI.mutation.getVouchers',
            'variables' =>
                [
                    'data' =>
                        [
                            'limit' => $limit,
                            'page' => $page,
                            'includeProducts' => $includeProducts,
                            'excludeProducts' => $excludeProducts,
                            /*
                            'sort' =>
                                [
                                    'field' => '',
                                    'order' => '',
                                ],
                            */
                            'filters' =>
                                [
                                    [
                                        'key' => 'productName',
                                        'value' => $productName,
                                    ],
                                    [
                                        'key' => 'country',
                                        'value' => $country,
                                    ],
                                    [
                                        'key' => 'price',
                                        'value' => $price,
                                    ],
                                    [
                                        'key' => 'minPrice',
                                        'value' => $minPrice,
                                    ],
                                    [
                                        'key' => 'maxPrice',
                                        'value' => $maxPrice,
                                    ],
                                    [
                                        'key' => 'currencyCode',
                                        'value' => $currencyCode,
                                    ],
                                ]
                        ]
                ],
        ];
        return $this->post($data);
    }

    public function getOrderHistory(string $startDate = '2010-01-01', string $endDate = '2030-01-01', int $limit = 1000, int $page = 0)
    {
        $data = [
            'query' => 'plumProAPI.mutation.getOrderHistory',
            'variables' =>
                [
                    'data' =>
                        [
                            'startDate' => $startDate,
                            'endDate' => $endDate,
                            'limit' => $limit,
                            'page' => $page,
                        ]
                ],
        ];
        return $this->post($data);
    }

    public function getFilters(string $filterGroupCode = null, string $includeFilters = null, string $excludeFilters = null)
    {
        $data = [
            'query' => 'plumProAPI.mutation.getFilters',
            'variables' =>
                [
                    'data' =>
                        [
                            'filterGroupCode' => $filterGroupCode,
                            'includeFilters' => $includeFilters,
                            'excludeFilters' => $excludeFilters,
                        ],
                ],
        ];
        return $this->post($data);
    }

    public function getReports(string $requestType = null, string $startDate = '2010-01-01', string $endDate = '2030-01-01', int $offset = 0, int $limit = 1000)
    {
        $data = [
            'query' => 'plumProAPI.mutation.getReports',
            'variables' =>
                [
                    'data' =>
                        [
                            'requestType' => $requestType,
                            'startDate' => $startDate,
                            'endDate' => $endDate,
                            'offset' => $offset,
                            'limit' => $limit,
                        ],
                ],
        ];
        return $this->post($data);
    }

    public function placeOrder(string $productId, int $denomination, string $poNumber = null, int $quantity = 1, string $email = null, string $contact = null, string $tag = null, bool $notifyAdminEmail = false, bool $notifyReceiverEmail = false,)
    {
        $data = [
            'query' => 'plumProAPI.mutation.placeOrder',
            'variables' =>
                [
                    'data' =>
                        [
                            'productId' => $productId,
                            'denomination' => $denomination,
                            'poNumber' => $poNumber,
                            'quantity' => $quantity,
                            'email' => $email,
                            'contact' => $contact,
                            'tag' => $tag,
                            'notifyAdminEmail' => $notifyAdminEmail ? 1 : 0,
                            'notifyReceiverEmail' => $notifyReceiverEmail ? 1 : 0,
                        ],
                ],
        ];
        return $this->post($data);
    }


    public function validateToken($urlPart = 'token')
    {
        Log::info('Xoxoday-API-Auth: Validating Token', ['url' => $this->host . $urlPart]);

        $response = Http::retry(3, 100)
            ->asJson()
            ->acceptJson()
            ->withHeaders([
                'Authorization' => 'Bearer ' . $this->getAccessToken()
            ])->get($this->host . 'token',);

        if($response->successful())
        {
            Log::debug('Xoxoday-API-Auth: Got response from validation', ['response-json' => $response->json()]);
        }
        else {
            Log::warning('Xoxoday-API-Auth: Got invalid response from validation', ['status' => $response->status(), 'response-json' => $response->json()]);
        }
        if($response->json('access_token') !== $this->getAccessToken())
        {
            // If the token doesn't match or isn't found to be valid, get a new one and run this function again
            $this->refreshToken();
            return $this->validateToken();
        }
        return true;
    }

    public function refreshToken($urlPart = 'token/user') : string
    {
        Log::info('Xoxoday-API-Auth: Sending post', ['url' => $this->host . $urlPart]);
        $data = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->getRefreshToken(),
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
        ];

        $response = Http::retry(3, 100)
            ->asJson()
            ->acceptJson()
            ->post($this->host . $urlPart, $data);

        if($response->successful())
        {
            Log::debug('Xoxoday-API-Auth: Got response from post', ['response-json' => $response->json()]);
            $this->saveAccessToken($response->json('access_token'), $response->json('expires_in'));
            return $this->saveRefreshToken($response->json('refresh_token'));
        }

        Log::warning('Xoxoday-API-Auth: Got invalid response from post', ['status' => $response->status(),'response-json' => $response->json()]);
        return 'invalid response';
    }

    /**
     *  Internal Supporting functions
     */
    protected function validateConfig()
    {
        // Stub
    }
    public function post(array $data = [], string $urlPart = 'api')
    {
        Log::info('Xoxoday-API: Sending post', ['url' => $this->host . $urlPart, 'data' => $data]);

        $data['tag'] = 'plumProAPI';

        $response = Http::retry(3, 100)
            ->asJson()
            ->acceptJson()
            ->withHeaders([
                'Authorization' => 'Bearer ' . $this->getAccessToken()
            ])->post($this->host . $urlPart, $data);

        if($response->successful())
        {
            Log::debug('Xoxoday-API: Got response from post', ['data' => $data, 'response-json' => $response->json()]);
        }
        else {
            Log::warning('Xoxoday-API: Got invalid response from post', ['status' => $response->status(), 'data' => $data, 'response-json' => $response->json()]);
        }
        return $response;
    }

    protected function getRefreshToken() : string
    {
        $token = Cache::tags('xoxoday')->get('refresh-token');
        if(is_null($token)){
            throw new InvalidConfigException("Xoxoday is missing it's refresh token, run php artisan xoxoday:install");
        }
        return $token;
    }
    public function saveRefreshToken(string $refreshToken)
    {
        Cache::tags('xoxoday')->forever('refresh-token', $refreshToken);
        return $this->getRefreshToken();
    }
    protected function getAccessToken() : string
    {
        if(!Cache::tags('xoxoday')->has('access-token')){
            $this->refreshToken();
        }
        return Cache::tags('xoxoday')->get('access-token');
    }
    protected function saveAccessToken(string $accessToken, int $expiresIn) : string
    {
        Cache::tags('xoxoday')->put('access-token', $accessToken, ($expiresIn-60));
        return $this->getAccessToken();
    }
}
