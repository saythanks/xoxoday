<?php

namespace SayThanks\Xoxoday\Console\Commands;

use Illuminate\Console\Command;
use SayThanks\Xoxoday\Xoxoday;

class InstallCommand extends Command
{

    protected $signature = 'xoxoday:install 
                            {--t | token= : The refresh token to be used}';

    protected $description = 'Initialise the auth flow with a refresh token that can be used to re-generate the access tokens';

    private string $refresh_token;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        if(!($this->refresh_token = (string)$this->option('token'))){
            $this->refresh_token = $this->ask('What is the Refresh Token?');
        }
        $xoxoday = resolve(Xoxoday::class);

        $xoxoday->saveRefreshToken($this->refresh_token);
        $xoxoday->refreshToken();

        return 0;
    }
}
