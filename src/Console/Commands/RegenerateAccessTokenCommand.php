<?php

namespace SayThanks\Xoxoday\Console\Commands;

use Illuminate\Console\Command;
use SayThanks\Xoxoday\Xoxoday;

class RegenerateAccessTokenCommand extends Command
{

    protected $signature = 'xoxoday:regenerate ';

    protected $description = 'Regenerate the access & refresh tokens from existing cached refresh token';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        resolve(Xoxoday::class)->refreshToken();

        return 0;
    }
}
