<?php

return

    /*
    |--------------------------------------------------------------------------
    | Xoxoday API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [
        'auth' => [
            'client_id' => env('XOXODAY_CLIENT_ID'),
            'client_secret' => env('XOXODAY_CLIENT_SECRET'),
        ],
        'host' => env('XOXODAY_HOST', 'https://stagingaccount.xoxoday.com/chef/v1/oauth/')
    ];