<?php

namespace SayThanks\Xoxoday;

use Illuminate\Support\ServiceProvider;
use SayThanks\Xoxoday\Console\Commands\InstallCommand;
use SayThanks\Xoxoday\Console\Commands\RegenerateAccessTokenCommand;

class XoxodayServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/xoxoday.php', 'xoxoday');
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                RegenerateAccessTokenCommand::class,
            ]);
        }
        $this->publishes(
            [
                __DIR__ . '/config/xoxoday.php' => config_path('xoxoday.php'),
            ], 'config');
    }


}