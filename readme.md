# Xoxoday Integration For Laravel

Provides API support for [Xoxoday](https://www.xoxoday.com/)

## About



## Installation

Add the repository to your composer respository section:
```json
"repositories": [
   {
      "type": "vcs",
      "url": "git@bitbucket.org:saythanks/xoxoday.git"
   }
],
```

Update your lockfile based on the repository change:
```sh
composer update
```
Require the package via composer:
```sh
composer require saythanks/xoxoday
```

## Configuration

Add your environment variables:
```

```

The defaults are set in `config/xoxoday.php`. Publish the config to copy the file to your own config:
```sh
php artisan vendor:publish --provider="SayThanks\Xoxoday\XoxodayServiceProvider"
```


## Usage
